function createCard(name, description, pictureUrl, startDate, endDate, locationName) {
    const formattedStartDate = new Date(startDate).toLocaleDateString();
    const formattedEndDate = new Date(endDate).toLocaleDateString();
  
    return `
      <div class="card">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${locationName}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer text-muted">
          <small>Start Date: ${formattedStartDate}</small><br>
          <small>End Date: ${formattedEndDate}</small>
        </div>
      </div>
    `;
  }
  
  window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences';
  
    try {
      const response = await fetch(url);
  
      if (!response.ok) {
        throw new Error('Failed to fetch conference data.');
      }
  
      const data = await response.json();
  
      const columns = document.querySelectorAll('.col');
  
      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
  
        if (!detailResponse.ok) {
          throw new Error(`Failed to fetch details for conference: ${conference.title}`);
        }
  
        const details = await detailResponse.json();
        const title = details.conference.title;
        const description = details.conference.description;
        const pictureUrl = details.conference.location.picture_url;
        const startDate = details.conference.starts;
        const endDate = details.conference.ends;
        const locationName = details.conference.location.name;
        const html = createCard(title, description, pictureUrl, startDate, endDate, locationName);
  
        const columnIndex = data.conferences.indexOf(conference) % columns.length;
  
        columns[columnIndex].innerHTML += html;
      }
    } catch (error) {
      console.error(error);
  
      const errorAlert = `
        <div class="alert alert-danger" role="alert">
          An error occurred while fetching data. Please try again later.
        </div>
      `;
  
      document.querySelector('#error-container').innerHTML = errorAlert;
    }
  });